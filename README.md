# README #

This repository consists of several websites for tracking UTA bus routes using google maps. So far I have added bus routes 2, 3, 6, 11, 209, 213.  To see the website in action, see here: http://www.sci.utah.edu/~crottman/uta.

### Summary ###

* An HTML page for each route that connects to google maps, plots the points, and plots the bus location
* A python script for each route to download the current xml ever few seconds
* A shell script that restarts all the scripts (I currently run this script every hour using crontab)

