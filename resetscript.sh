#!/bin/bash

cd /usr/sci/www/crottman/uta/

pkill -f get\[0-9\]+xml.py

python get2xml.py & disown
python get3xml.py & disown
python get6xml.py & disown
python get11xml.py & disown
python get209xml.py & disown
python get213xml.py & disown
