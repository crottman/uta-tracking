function getGpsTime(d) {
    xmlhttp=new XMLHttpRequest();
    xmlhttp.open("GET","rt209data.xml",false);
    xmlhttp.send();
    xmlDoc = xmlhttp.responseXML;

    if (xmlDoc != null) {
        var x = xmlDoc.getElementsByTagName("ResponseTimestamp");
        document.getElementById("209_date").innerHTML = 'Raw date from (copied) UTA XML:  ' + x[0].childNodes[0].nodeValue;

        var timestring=x[0].childNodes[0].nodeValue
        var year = parseInt(timestring.substring(0,4), 10);
        var month = parseInt(timestring.substring(5,7), 10) -1; // month is 0-11
        var day = parseInt(timestring.substring(8,10), 10);
        var hour = parseInt(timestring.substring(11,13), 10);
        var min = parseInt(timestring.substring(14,16), 10);
        var sec = parseInt(timestring.substring(17,19), 10);

        var d = new Date(year, month, day, hour, min, sec, 0);
        var num2 = 1+2;
    }
    return d;                   // if undefined, returns the input value
}

function subtractTime() {
    var d1 = new Date();
    document.getElementById("curr_date").innerHTML='Raw date from computer:  ' + d1;
    var a = 'breakline'
    d2 = getGpsTime(d2);
    var b = 'breakline'
    if (d2.getTime() != 0) {
        var num2 = 1+4;
        if (d2.getTime() > d1.getTime()){
            document.getElementById("subtraction").innerHTML=' gps bus data is up to date! ';
        }
        else {
            var msdiff = d1.getTime()-d2.getTime(); // difference in milliseconds
            var ddiff = new Date(25200000+msdiff)   // correct for time zone
            var year = ddiff.getYear() - 70;        // correct for 1970 start year
            var month = ddiff.getMonth();
            var day = ddiff.getDate() - 1; // correct for first day of month
            var hour = ddiff.getHours();
            var min = ddiff.getMinutes();
            var sec = ddiff.getSeconds();
            // output string based on difference
            var outstring = '.'
            if (year > 0) {
                outstring = ' gps data is behind by ' + year + ' year(s), ' +
                    month + ' month(s), ' + day + ' day(s) ' +
                    hour + ' hr, ' + min + ' min ' +
                    sec + ' sec ';
            }
            else if (month > 0) {
                outstring = ' gps data is behind by ' +
                    month + ' month(s), ' + day + ' day(s) ' +
                    hour + ' hr, ' + min + ' min ' +
                    sec + ' sec ';
            }
            else if (day > 0) {
                outstring = ' gps data is behind by ' + day + ' day(s) ' +
                    hour + ' hr, ' + min + ' min ' +
                    sec + ' sec ';
            }
            else if (hour > 0) {
                outstring = ' gps data is behind by ' +
                    hour + ' hr, ' + min + ' min ' +
                    sec + ' sec ';
            }
            else if (min > 0) {
                outstring = ' gps data is behind by ' + min + ' min ' +
                    sec + ' sec ';
            }
            else if (sec >= 0) {
                outstring = ' gps data is behind by ' + sec + ' sec ';
            }
            // set HTML
            document.getElementById("subtraction").innerHTML=outstring;
            var num3 = 2+7;
        }
    }
}

var d2 = new Date(0);           // set initial value for d2
setInterval('subtractTime()',1000);
